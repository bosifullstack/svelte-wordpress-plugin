const sveltePreprocess = require("svelte-preprocess");

module.exports.preprocess = sveltePreprocess({
  scss: {
    includePaths: ["src"]
    // data: `@import 'style/core/grid.scss';
    // @import 'style/core/variables.scss';
    // @import 'style/core/fonts.scss';
    // @import 'style/core/recipes.scss';
    // @import 'style/global.scss';`
  },
  postcss: {
    plugins: [require("autoprefixer")]
  }
});
