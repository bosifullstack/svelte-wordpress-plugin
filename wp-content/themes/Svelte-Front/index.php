<!DOCTYPE html>                                                                                                                             
<html <?php language_attributes(); ?>>                                                                                                      
        <head>                                                                                                                              
                <meta charset="<?php bloginfo( 'charset' ); ?>">                                                                            
                <meta name="viewport" content="width=device-width,initial-scale=1">                                                         
                <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/public/favicon.png">                                                                                                    
                <link rel='stylesheet' href='<?php echo get_template_directory_uri(); ?>/public/bundle.css'>                     

                <title><?php bloginfo('name')?></title>                                                                                                                                                                                           
        </head>                                                                                                                             
        <body>                                                                                                                              
                                                                                                                                            
                                                                                                                                            
                                                                                                                                            
                                                                                                                                            
        <script src='<?php echo get_template_directory_uri(); ?>/public/bundle.js'></script>                                        
        
        <footer>
        <script src="wp-content/themes/Svelte-Front/public/dist/js/alldep.min.js"></script>
        <script src="wp-content/themes/Svelte-Front/public/dist/js/scripts.js"></script>

        </footer>                                                                                                               
        </body>                                                                                                                             
</html>                                                                                                                                     
