// This is the code used in video "How To Add Svelte To Your Site?"
// Check out the video here: https://www.youtube.com/watch?v=ZL7mKFQHSAY

const gulp = require("gulp");
const sourcemaps = require("gulp-sourcemaps");
const concat = require("gulp-concat");
const rename = require("gulp-rename");

gulp.task("font", function() {
  return gulp.src("src/webfonts/**/*").pipe(gulp.dest("public/dist/webfonts/"));
});
gulp.task("iconfont", function() {
  return gulp
    .src("node_modules/@fortawesome/fontawesome-free/webfonts/*")
    .pipe(gulp.dest("public/dist/webfonts"));
});
gulp.task("lgfont", function() {
  return gulp
    .src("node_modules/lightgallery/dist/fonts/*")
    .pipe(gulp.dest("public/dist/fonts"));
});

gulp.task("cssdep", function() {
  return gulp
    .src([
      "node_modules/bootstrap/dist/css/bootstrap-grid.min.css",
      "node_modules/swiper/dist/css/swiper.min.css",
      "node_modules/lightgallery/dist/css/lightgallery.min.css",
      "node_modules/@fortawesome/fontawesome-free/css/all.min.css"
    ])
    .pipe(sourcemaps.init())
    .pipe(concat("alldep.css"))
    .pipe(rename({ suffix: ".min" }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest("public/dist/css"));
});

gulp.task("jsdep", function() {
  return gulp
    .src([
      "node_modules/jquery/dist/jquery.slim.min.js",
      "node_modules/bootstrap/dist/js/bootstrap.min.js",
      "node_modules/popper.js/dist/umd/popper.min.js",
      "node_modules/swiper/dist/js/swiper.min.js",
      "node_modules/lightgallery/dist/js/lightgallery.min.js"
    ])
    .pipe(sourcemaps.init())
    .pipe(concat("alldep.js"))
    .pipe(rename({ suffix: ".min" }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest("public/dist/js"));
});

gulp.task(
  "default",
  gulp.series("font", "iconfont", "lgfont", "jsdep", "cssdep")
);
