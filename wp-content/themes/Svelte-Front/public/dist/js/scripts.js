$(document).ready(function() {
  var swiper = new Swiper("#ferramentas-solucoes .swiper-container", {
    slidesPerView: "auto",
    spaceBetween: 10,
    pagination: {
      el: ".swiper-pagination",
      clickable: true
    },
    navigation: {
      nextEl: "#ferramentas-solucoes .btn-next",
      prevEl: "#ferramentas-solucoes .btn-prev"
    }
  });

  var swiper = new Swiper("#nossos-diferenciais .swiper-container", {
    slidesPerView: "auto",
    spaceBetween: 25,
    pagination: {
      el: ".swiper-pagination",
      clickable: true
    },
    navigation: {
      nextEl: "#nossos-diferenciais .btn-next",
      prevEl: "#nossos-diferenciais .btn-prev"
    }
  });

  $("header .menu__button-div .btn").click(function() {
    $(".menu").css("right", "0");
    $(".menu").addClass("box-shadow");
    $("#header-overlay").show();
    $("body").addClass("disable-scroll");
  });

  $("#header-overlay").click(function() {
    $(".menu").css("right", "calc(25% - 100%)");
    $(".menu").removeClass("box-shadow");
    $("#header-overlay").hide();
    $("body").removeClass("disable-scroll");
  });

  $(".funcionalidades-360__opt").each(function(index, element) {
    $(this).click(function() {
      $(".funcionalidades-360__opt").each(function(index3, element3) {
        if (index == index3) {
          $(this).addClass("active");
        } else {
          $(this).removeClass("active");
        }
      });

      $(".funciona").each(function(index2, element2) {
        if (index == index2) {
          $(this).show();
        } else {
          $(this).hide();
        }
      });
    });
  });

  $(".trabalhe-conosco__menu button").each(function(index, element) {
    $(this).click(function() {
      $(".trabalhe-conosco__menu button").each(function(index3, element3) {
        if (index == index3) {
          $(this).addClass("active");
        } else {
          $(this).removeClass("active");
        }
      });

      var area = $(this).html();
      $(".trabalhe-conosco__card .area").each(function() {
        if (
          $(this)
            .html()
            .trim() == area
        ) {
          $(this)
            .parent()
            .parent()
            .parent()
            .show();
        } else {
          $(this)
            .parent()
            .parent()
            .parent()
            .hide();
        }

        if (area == "Todas") {
          $(this)
            .parent()
            .parent()
            .parent()
            .show();
        }
      });
    });
  });
});
